variable "compartment_id" {
  description = "(Required) (Updatable) The OCID of the compartment."
  type        = string
  default     = ""
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(any)
  default     = {}
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = {}
}

variable "display_name" {
  description = "A user-friendly name. Does not have to be unique, and it's changeable."
  type        = string
  default     = ""
}

variable "instance_display_name" {
  description = "A user-friendly name. Does not have to be unique, and it's changeable."
  type        = string
  default     = ""
}

variable "vcn_id" {
  description = "(Required) The OCID of the virtual cloud network (VCN) in which to create the cluster."
  type        = string
  default     = ""
}

variable "ingress_security_rules" {
  description = "List of ingress security rules to create"
  type        = any
  default     = {}
}

variable "egress_security_rules" {
  description = "List of egress security rules to create"
  type        = any
  default     = {}
}
