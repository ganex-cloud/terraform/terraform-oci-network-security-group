module "nsg_web-servers" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-network-security-group.git?ref=master"
  compartment_id = module.compartiment_infra.compartment_id
  display_name   = "Web Servers"
  vcn_id         = module.vcn.vcn_id
  ingress_security_rules = [
    {
      description            = "Allow HTTP"
      source                 = "0.0.0.0/0"
      source_type            = "CIDR_BLOCK"
      protocol               = 6
      stateless              = false
      destination_port_range = 80
      source_port_range      = 80
    },
    {
      description            = "Allow HTTPS"
      source                 = "0.0.0.0/0"
      source_type            = "CIDR_BLOCK"
      protocol               = 6
      stateless              = false
      destination_port_range = 443
      source_port_range      = 443
    },
  ]
  egress_security_rules = [
    {
      description      = "Allow all traffic"
      destination      = "0.0.0.0/0"
      destination_type = "CIDR_BLOCK"
      protocol         = "all"
      stateless        = false
    }
  ]
}
