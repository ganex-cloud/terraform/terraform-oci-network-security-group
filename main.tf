resource "oci_core_network_security_group" "this" {
  compartment_id = var.compartment_id
  display_name   = var.display_name
  vcn_id         = var.vcn_id
}

resource "oci_core_network_security_group_security_rule" "egress" {
  count                     = length(var.egress_security_rules)
  network_security_group_id = oci_core_network_security_group.this.id
  direction                 = "EGRESS"
  description               = lookup(var.egress_security_rules[count.index], "description")
  destination               = lookup(var.egress_security_rules[count.index], "destination")
  destination_type          = lookup(var.egress_security_rules[count.index], "destination_type")
  protocol                  = lookup(var.egress_security_rules[count.index], "protocol")
  stateless                 = lookup(var.egress_security_rules[count.index], "stateless")
}

resource "oci_core_network_security_group_security_rule" "ingress" {
  count                     = length(var.ingress_security_rules)
  network_security_group_id = oci_core_network_security_group.this.id
  direction                 = "INGRESS"
  description               = lookup(var.ingress_security_rules[count.index], "description")
  protocol                  = lookup(var.ingress_security_rules[count.index], "protocol")
  source                    = lookup(var.ingress_security_rules[count.index], "source")
  source_type               = lookup(var.ingress_security_rules[count.index], "source_type")
  stateless                 = lookup(var.ingress_security_rules[count.index], "stateless")

  dynamic "tcp_options" {
    for_each = var.ingress_security_rules[count.index].protocol == 6 ? list(1) : []

    content {
      dynamic "destination_port_range" {
        for_each = var.ingress_security_rules[count.index].destination_port_range == "" ? [] : list(1)
        content {
          min = var.ingress_security_rules[count.index].destination_port_range
          max = var.ingress_security_rules[count.index].destination_port_range
        }
      }
      dynamic "source_port_range" {
        for_each = var.ingress_security_rules[count.index].source_port_range == "" ? [] : list(1)
        content {
          min = var.ingress_security_rules[count.index].source_port_range
          max = var.ingress_security_rules[count.index].source_port_range
        }
      }
    }
  }
  dynamic "udp_options" {
    for_each = var.ingress_security_rules[count.index].protocol == 17 ? list(1) : []

    content {
      dynamic "destination_port_range" {
        for_each = var.ingress_security_rules[count.index].destination_port_range == "" ? [] : list(1)
        content {
          min = var.ingress_security_rules[count.index].destination_port_range
          max = var.ingress_security_rules[count.index].destination_port_range
        }
      }
      dynamic "source_port_range" {
        for_each = var.ingress_security_rules[count.index].source_port_range == "" ? [] : list(1)
        content {
          min = var.ingress_security_rules[count.index].source_port_range
          max = var.ingress_security_rules[count.index].source_port_range
        }
      }
    }
  }
}
